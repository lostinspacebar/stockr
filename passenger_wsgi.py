# Make sure we have some paths being looked at
# that are part of our python environment
import sys
sys.path.append("/home/adityagaddam/stockr.lab.lostinspacebar.com/")
sys.path.append("/home/adityagaddam/stockr.lab.lostinspacebar.com/stockr/")
sys.path.append("/home/adityagaddam/stockr.lab.lostinspacebar.com/stockr/controllers/")
sys.path.append("/home/adityagaddam/python-env/lib/python2.5/site-packages/")

# Web.Py
import web

# SqlAlchemy
from sqlalchemy.orm import scoped_session, sessionmaker

# Stockr Application
from stockr.models import engine

# Setup URLs
# TODO: Figure out how to import these controller classes nicely
#		instead of having to import by their full names
urls = (
	'/', "stockr.controllers.index.Index",
	'/login', "stockr.controllers.login.Index"
)


def load_sqla(handler):
	"""
		Initialize SQLAlchemy for use with the rest of the app
	"""
	web.ctx.orm = scoped_session(sessionmaker(bind=engine))
	try:
		return handler()
	except web.HTTPError:
		web.ctx.orm.commit()
		raise
	except:
		web.ctx.orm.rollback()
		raise
	finally:
		web.ctx.orm.commit()
		# If the above alone doesn't work, uncomment 
		# the following line:
		#web.ctx.orm.expunge_all() 


# Setup application
app = web.application(urls, globals())
app.add_processor(load_sqla)

# Run!
if __name__ == "__main__": 
	app.run()
else:
	application = app.wsgifunc()
