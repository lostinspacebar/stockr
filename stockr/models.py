# Web.Py
import web

# Sql Alchemy
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from sqlalchemy import Column, Integer, String, or_, and_

# Hashlib for passwords
import hashlib

# Setup the base class for SQLAlchemy declarations
Base = declarative_base()

# Table Prefix for Stockr
table_prefix = "stockr_"

# Connect to the database
engine = create_engine("mysql://lis_lab_db_user:QhSt5XtanSLX@mysql.lostinspacebar.com/lis_lab_stockr_db")

### Setup some shortcuts for ORM objects by extending the base class
### Kudos to http://www.brankovukelic.com/post/1312435119/poor-mans-web-py-orm-dead

def getall(cls, order=None):
	"""
		Return all objects for the specified class with the specified ordering
		
		Keyword arguments:
		cls -- Class/Type
		order -- Order clause
	"""
	return web.ctx.orm.query(cls).order_by(order).all()

def getpage(cls, order=None, page=1, per_page=30):
	"""
		Return a subset of objects for the specified class with the specified
		ordering starting at a specified page, returning at most 'per_page'
		number of items.
		
		Keyword arguments:
		cls -- Class/Type
		order -- Order clause
		page -- Page number to start the listing at
		per_page -- Maximum number of items to include in the listing
	"""
	offset = (page - 1) * per_page
	return web.ctx.orm.query(cls).order_by(order).offset(offset).limit(per_page).all()
	
def getquery(cls):
	return web.ctx.orm.query(cls)

# Connect shortcuts to the base class
Base.all = classmethod(getall)
Base.by_page = classmethod(getpage)
Base.query = classmethod(getquery)

### Models

class User(Base):
	"""
		User Model
		
		Represents a user in the Stockr database. Permissions for user are 
		managed via groups and roles.
	"""
	# Build table name with the global table prefix
	__tablename__ = table_prefix + "users"

	# Setup Columns
	id = Column(Integer, primary_key=True)
	username = Column(String)
	fullname = Column(String)
	password = Column(String)
	email = Column(String)
	avatar = Column(String)

	def __init__(self, username, fullname, password, email, avatar = None):
		self.username = username
		self.fullname = fullname
		self.password = hashlib.sha1(password).hexdigest()
		self.email = email
		self.avatar = avatar
	
	@staticmethod
	def try_login(username, password):
		"""
			Try to login with the specified username and password without 
			doing anything to the session. Return True if the username/password
			combination is valid. False otherwise
			
			Keyword arguments:
			username -- Username to authenticate with
			password -- Password (pre-SHA1) to authenticate with
		"""
		# Passwords are actually stored in sha1
		password = hashlib.sha1(password).hexdigest()
		
		# Get any users with the username/password combo
		users = User.query().filter(and_(User.username == username, 
										 User.password == password)).all()
		
		# Should have exactly 1 user with the specified username/password
		if(len(users) == 1):
			return True
		else:
			return False

	def __repr__(self):
		return "<User('%s','%s', '%s')>" % (self.username, self.fullname, self.email)
	

