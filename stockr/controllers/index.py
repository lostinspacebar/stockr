import web
from ..models import User

# Index page. This is the main page we see
# when we go to stockr
class Index:
	def GET(self):
		web.header('Content-Type', 'text/html')
		params = web.input()
		users = User.by_page(page=1)
		return "Hello World " + str(len(users))
