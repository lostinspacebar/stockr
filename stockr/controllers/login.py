# Web.py
import web

# SqlAlchemy
from sqlalchemy import and_, or_

# Hashlib for passwords
import hashlib

# Import User model for auth stuff
from ..models import User

# Main Login page
class Index:
	def GET(self):
		web.header('Content-Type', 'text/html')
		params = web.input()
		return str(User.try_login(params["username"], params["password"]))
